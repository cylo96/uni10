#include <string>
#include <regex>

#include "test_tool/test_opts.h"
#include <boost/program_options.hpp>

using namespace std;
using namespace boost::program_options;

Opts::Opts() {
	_desc.add_options()
		("help,h", "Display this help message")
		("size,s", value<string>(),
		 "Specify the size of the matrix")
		("ntest,n", value<int>()->default_value(1),
		 "Specify the number of test case")
		("inplace,i", "Test the inplace version");
}

int Opts::parse(int argc, char **argv)
{
    try {
        store(command_line_parser(argc, argv).options(_desc).run(), _vmap);
        notify(_vmap);

        if (_vmap.count("inplace")) {
            _inplace = true;
        } else {
            _inplace = false;
        }

        if (_vmap.count("help")) {
            cout << _desc;
            return 0;
        }

        if (_vmap.count("size")) {
            const auto matrix_size = _vmap["size"].as<string>();
            regex matrix_size_re("([[:digit:]]+)x([[:digit:]]+)");
            smatch matrix_size_match;

            if (!regex_match(matrix_size, matrix_size_match, matrix_size_re))
                throw validation_error{validation_error::invalid_option_value, matrix_size, "size"};

            _Rnum = stoi(matrix_size_match[1].str());
            _Cnum = stoi(matrix_size_match[2].str());
            _ntest = _vmap["ntest"].as<int>();

            return 1;
        }
    } catch (const error &err) {
        cerr << err.what() << endl;
        exit(EXIT_FAILURE);
    }

	cout << _desc;
	return 0;
}

unsigned int Opts::Rnum()
{
    return _Rnum;
}

unsigned int Opts::Cnum()
{
    return _Cnum;
}

unsigned int Opts::ntest()
{
    return _ntest;
}

bool Opts::inplace()
{
    return _inplace;
}
