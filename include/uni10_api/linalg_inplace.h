#ifndef __UNI10_LINALG_INPLACE_API_H__
#define __UNI10_LINALG_INPLACE_API_H__

#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_dotargs.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_dots.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_dot.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_resize.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_qr.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_rq.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_lq.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_ql.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_qdr.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_ldq.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_qdr_cpivot.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_svd.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_sdd.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_eigh.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_eig.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_inverse.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_transpose.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_dagger.h"
#include "uni10_api/uni10_linalg_inplace/uni10_linalg_inplace_conj.h"

#endif
