PROJECT := uni10
CONFIG_FILE := make.inc

##########################################
# Color
COLOR_REST=\e[0m
COLOR_GREEN=\e[0;32m
COLOR_RED=\e[0;31m
COLOR_PURPLE=\e[0;35m
COLOR_YELLOW=\e[0;33m

## Explicitly check for the config file, otherwise make -k will proceed anyway.
ifeq ($(wildcard $(CONFIG_FILE)),)
$(error $(CONFIG_FILE) not found.)
endif
include $(CONFIG_FILE)

## Identify the version name
VERSION := $(LNPACKAGE)_$(CALARCH)
UNI10_VERSION := $(addprefix $(PROJECT)_, $(VERSION))

## The direction to save the objects.

UNI10_INSTALL_PATH := uni10/
ifneq ($(UNI10_INSTALL_PREFIX),)
	UNI10_INSTALL_PATH := $(UNI10_INSTALL_PREFIX)/
endif

SRC_DIRS := $(shell find src/ -type d -exec bash -c "find {} -maxdepth 1 \
	\( -name '*.cpp' -o -name '*.cu' \) | grep -q ." \; -print)

INC_DIRS := $(shell find src/ -type d -exec bash -c "find {} -maxdepth 1 \
	\( -name '*.h' -o -name '*.hpp' \) | grep -q ." \; -print)

INC_DIRS += $(shell find src/ -type d -exec bash -c "find {} -maxdepth 1 \
	\( -name '*.cuh' \) | grep -q ." \; -print)

# Names of the static and dynamic shared libraries.
UNI10_LIB_PATH := $(addprefix $(UNI10_INSTALL_PATH),lib)
UNI10_INC_PATH := $(addprefix $(UNI10_INSTALL_PATH),include)
STATIC_NAME    := $(addprefix $(UNI10_LIB_PATH)/,lib$(UNI10_VERSION).a)
DYNAMIC_NAME   := $(addprefix $(UNI10_LIB_PATH)/,lib$(UNI10_VERSION).so)
#UNI10_GTEST_PATH := $(addprefix $(UNI10_INSTALL_PATH),gtest)

##############################
# Find all source files
##############################

# CXX_SRCS are the source files excluding the test ones.
#
CXX_SRCS :=
#Find the auxiliary files
CXX_SRCS += $(shell find src/ -maxdepth 1 -name "*.cpp")
CXX_SRCS += $(shell find src/uni10_env_info/uni10_$(VERSION) -name "*$(VERSION).cpp")
# Find UNI10_API FILES
CXX_SRCS += $(shell find src/uni10_api -name "*.cpp")
# Find the others sources of the specified versions.
CXX_SRCS += $(shell find src/uni10_$(VERSION) -name "*.cpp")

# CU_SRCS
CU_SRCS :=
ifeq ($(CALARCH), gpu)
	#Find the auxiliary files
	CU_SRCS += $(shell find src/uni10_env_info/uni10_$(VERSION) -name "*.cu")
	# Find the others sources of the specified versions.
	CU_SRCS += $(shell find src/uni10_$(VERSION) -name "*.cu")
endif

ifeq ($(GTEST), 1)
	CXX_SRCS += $(GTEST_SRCS)
endif

##############################
# Derive generated files
##############################

# CXX objects
CXX_OBJS := $(addprefix $(UNI10_INSTALL_PATH), ${CXX_SRCS:.cpp=.o})

# CU objects
GPU_TGT :=
CU_OBJS :=
ifeq ($(CALARCH), gpu)
	GPU_TGT += $(UNI10_INSTALL_PATH)uni10_GPU.o
	CU_OBJS += $(addprefix $(UNI10_INSTALL_PATH), ${CU_SRCS:.cu=.o})
endif

GTEST_BIN := $(addprefix $(UNI10_INSTALL_PATH), ${GTEST_SRCS:.cpp=.bin})

##############################
# Derive compiler warning dump locations
##############################

WARNS_EXT := warnings.txt

CXX_WARNS := $(addprefix $(UNI10_INSTALL_PATH), ${CXX_SRCS:.cpp=.o.$(WARNS_EXT)})

##############################
# Set build directories
##############################

INCLUDE_DIRS := ./include

ALL_BUILD_DIRS := $(sort $(UNI10_INSTALL_PREFIX) $(UNI10_LIB_PATH) \
	$(addprefix $(UNI10_INSTALL_PATH), $(SRC_DIRS)))

##############################
# Configure build
##############################
#
# Determine platform
UNAME := $(shell uname -s)
ifeq ($(UNAME), Linux)
	LINUX := 1
else ifeq ($(UNAME), Darwin)
	OSX := 1
endif

# Linux
ifeq ($(LINUX), 1)
	COMMON_FLAGS += -DLINUX
	ifeq ($(CXX), g++)
		CXX = /usr/bin/g++
		GCCVERSION := $(shell $(CXX) -dumpversion | cut -f1,2 -d.)
		# older versions of gcc are too dumb to build boost with -Wuninitalized
		ifeq ($(shell echo | awk '{exit $(GCCVERSION) < 4.6;}'), 1)
			WARNINGS  += -Wno-uninitialized
		endif
	endif
	ifeq ($(CXX), icpc)
		CXX = /opt/intel/bin/icpc
	endif
endif

INCLUDE_DIRS += $(BLAS_INCLUDE) $(CUDA_INCLUDE) $(HPTT_INCLUDE) $(TCL_INCLUDE)
LIBRARY_DIRS += $(BLAS_LIB) $(CUDA_LIB) $(HPTT_LIB) $(TCL_LIB)

####################
# isolate GPU compiler part to generate single GPU object (target)
# and link via custom CXX
####################
GPU_INCLUDE_DIRS :=
GPU_LIBRARY_DIRS :=
NVCC_ALLCCFLAGS  :=
NVCC_ALLLDFLAGS  :=
ifeq ($(CALARCH), gpu)
	GPU_INCLUDE_DIRS += $(addprefix -I,$(CUDA_INCLUDE) $(INCLUDE_DIRS))
	GPU_LIBRARY_DIRS += $(addprefix -L,$(CUDA_LIB) $(LIBRARY_DIRS))
	NVCC_ALLCCFLAGS += -Wno-deprecated-gpu-targets -Xcompiler "$(UNI10CXXFLAGS) $(NVCC_CCFLAGS) $(GPU_INCLUDE_DIRS)"
	NVCC_ALLLDFLAGS += $(addprefix -Xlinker ,$(GPU_LIBRARY_DIRS))
ifeq ($(GPU_DEBUG), 1)
	NVCCFLAGS += -DGPUDEBUG=$(GPU_DEBUG)
endif

endif

#
LIBRARY_DIRS += $(UNI10_LIB_PATH)
#
# Automatic dependency generation (nvcc is handled separately)
CXXFLAGS += -MMD -MP

# Complete build flags.
COMMON_FLAGS += $(foreach includedir,$(INCLUDE_DIRS),-I$(includedir))
CXXFLAGS     += $(COMMON_FLAGS) $(WARNINGS)

LDFLAGS += $(foreach librarydir,$(LIBRARY_DIRS),-L$(librarydir)) \
		$(foreach library,$(LDLIBRARIES),-l$(library))

#LINKFLAGS += -lcudart

GTESTLDLIBS = gtest gtest_main pthread
GTESTLDFLAGS += $(foreach gtestlib,$(GTESTLDLIBS),-l$(gtestlib))

GPU_ARCH ?= Kepler

NV_SM  :=
GPUMSG :=
ifeq ($(CALARCH), gpu)
	# NVCC options for the different cards
	# First, add smXX for architecture names


	ifneq ($(findstring Fermi, $(GPU_ARCH)),)
		GPU_ARCH += sm20
	endif
	ifneq ($(findstring Kepler, $(GPU_ARCH)),)
		GPU_ARCH += sm30
	endif
	ifneq ($(findstring Maxwell, $(GPU_ARCH)),)
		GPU_ARCH += sm50 sm52
	endif
	ifneq ($(findstring Pascal, $(GPU_ARCH)),)
		GPU_ARCH += sm60 sm61
	endif

	ifneq ($(findstring sm20, $(GPU_ARCH)),)
	    NV_SM    += -arch=sm_20
	    GPUMSG += "[Warning][Deprecated Fermi ARCH]"
	endif
	ifneq ($(findstring sm30, $(GPU_ARCH)),)
	    NV_SM    += -arch=sm_30
	endif
	ifneq ($(findstring sm35, $(GPU_ARCH)),)
	    NV_SM    += -arch=sm_35
	endif
	ifneq ($(findstring sm50, $(GPU_ARCH)),)
	    NV_SM    += -arch=sm_50
	endif
	ifneq ($(findstring sm52, $(GPU_ARCH)),)
	    NV_SM    += -arch=sm_52
	endif
	ifneq ($(findstring sm60, $(GPU_ARCH)),)
	    NV_SM    += -arch=sm_60
	endif
	ifneq ($(findstring sm61, $(GPU_ARCH)),)
	    NV_SM    += -arch=sm_61
	endif
	ifeq ($(NV_SM),)
	    $(error GPU_ARCH, currently $(GPU_ARCH), must contain one or more of Fermi, Kepler, Maxwell, Pascal, or sm{20,30,35,50,52,60,61}. Please edit your make.inc file)
	endif
	NVCCFLAGS += $(NV_SM)

endif

# 'superclean' target recursively* deletes all files ending with an extension
# in $(SUPERCLEAN_EXTS) below.  This may be useful if you've built older
# versions of Caffe that do not place all generated files in a location known
# to the 'clean' target.
#
# 'supercleanlist' will list the files to be deleted by make superclean.
#
# * Recursive with the exception that symbolic links are never followed, per the
# default behavior of 'find'.
SUPERCLEAN_EXTS := .so .a .o

##############################
# Define build targets
##############################

TARGET := lib include

ifeq ($(GTEST), 1)
	TARGET += gtest
endif

all: $(TARGET)

lib: $(STATIC_NAME) $(DYNAMIC_NAME)

gtest: $(GTEST_BIN)

include: $(UNI10_INSTALL_PATH)
	@ echo CP -r include $<
	$(Q)cp -r include $<

$(ALL_BUILD_DIRS):
	@ mkdir -p $@

$(DYNAMIC_NAME): $(CXX_OBJS) $(GPU_TGT) $(CU_OBJS)| $(UNI10_LIB_PATH)
	@ echo -e "$(COLOR_YELLOW)LD$(COLOR_REST)" -o $@
	$(Q)$(CXX) -shared -o $@ $^ $(LINKFLAGS) $(LDFLAGS) $(DYNAMIC_FLAGS)

$(STATIC_NAME): $(CXX_OBJS) $(GPU_TGT) $(CU_OBJS) | $(UNI10_LIB_PATH)
	@ echo -e "$(COLOR_YELLOW)AR$(COLOR_REST)" -o $@
	$(Q)ar rcs $@ $^

$(UNI10_INSTALL_PATH)%.o: %.cpp | $(ALL_BUILD_DIRS)
	@ echo -e "$(COLOR_GREEN)CXX$(COLOR_REST)" $<
	$(Q)$(CXX) $< $(CXXFLAGS) -c -o $@ 2> $@.$(WARNS_EXT) \
		|| (cat $@.$(WARNS_EXT); exit 1)
	@ cat $@.$(WARNS_EXT)


### GPU Compiling & linking
$(GPU_TGT): $(CU_OBJS)
ifeq ($(CALARCH), gpu)
	@ echo -e "$(COLOR_YELLOW)DEVL$(COLOR_REST)" $@
	$(Q)$(NVCC) -dlink $^ $(NVCCFLAGS) $(NVCC_ALLCCFLAGS) $(NVCC_ALLLDFLAGS)  -o $@
endif
$(UNI10_INSTALL_PATH)%.o: %.cu | $(ALL_BUILD_DIRS)
ifeq ($(CALARCH), gpu)
	@ echo -e "$(COLOR_PURPLE)CU$(COLOR_RED)$(GPUMSG)$(COLOR_REST)" $<
	$(Q)$(NVCC) $(NVCCFLAGS) $(NVCC_ALLCCFLAGS) -dc $<  -o $@ 2> $@.$(WARNS_EXT) \
		|| (cat $@.$(WARNS_EXT); exit 1)
	@ cat $@.$(WARNS_EXT)
endif


### Testing
$(GTEST_BIN): $(GTEST_SRCS)
	@ echo CXX $< -o $@
	$(Q)$(CXX) $(CXXFLAGS) $< -o $@ $(STATIC_NAME) $(LDFLAGS) $(GTESTLDFLAGS)

clean:
	@- $(RM) -rf $(ALL_BUILD_DIRS)
	@- $(RM) -rf docs/latex docs/html docs/man

clean_header:
	@- $(RM) -rf $(UNI10_INC_PATH)

clean_doxygen:
	@- $(RM) -rf docs/latex docs/html docs/man

print-%  : ; @echo $* = $($*)
