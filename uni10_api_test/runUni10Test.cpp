#include "testNetwork.h"
#include "testBond.h"
#include "testMatrix.h"
#include "testMatrixd.h"
#include "testMatrixz.h"
#include "testTensor.h"
#include "testQnum.h"

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();

}
