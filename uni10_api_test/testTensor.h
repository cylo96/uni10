#ifndef _TESTTENSOR_H_
#define _TESTTENSOR_H_

#include "gtest/gtest.h"

#include <vector>
#include <random>

#include "uni10.hpp"

using namespace uni10;


TEST(tensor,Defaultconstructor){

	UniTensor<double> T;

	EXPECT_EQ(1,T.ElemNum());

}
TEST(tensor,constructorFrombonds){
	Bond bdi_2(BD_IN,2);
	Bond bdi_3(BD_IN,3);
	Bond bdi_4(BD_OUT,4);

	std::vector<Bond> bonds;
	bonds.push_back(bdi_2);
	bonds.push_back(bdi_3);
	bonds.push_back(bdi_4);

	UniTensor<double> T(bonds);

	EXPECT_EQ(24,T.ElemNum());
	EXPECT_EQ(3,T.BondNum());
	EXPECT_EQ(bdi_2,T.bond(0));
	EXPECT_EQ(bdi_3,T.bond(1));
	EXPECT_EQ(bdi_4,T.bond(2));
}
TEST(tensor,constructorwithElem){
	Bond bdi_2(BD_IN,2);
	Bond bdi_3(BD_IN,3);
	Bond bdi_4(BD_OUT,4);

	std::vector<Bond> bonds;
	bonds.push_back(bdi_2);
	bonds.push_back(bdi_3);
	bonds.push_back(bdi_4);

	UniTensor<double> T(bonds);

	std::vector<double> Telem(24,0);
	for(size_t i=0; i < Telem.size(); i++){
		Telem[i]=i;
	}

	T.SetElem(Telem);
	EXPECT_EQ(24,T.ElemNum());
	EXPECT_EQ(3,T.BondNum());
	EXPECT_EQ(bdi_2,T.bond(0));
	EXPECT_EQ(bdi_3,T.bond(1));
	EXPECT_EQ(bdi_4,T.bond(2));
	for(size_t i=0; i < 24; i++){
		EXPECT_EQ(T[i],Telem[i]);
	}
}
TEST(tensor,Copyconstructor){
	Bond bdi_2(BD_IN,2);
	Bond bdi_3(BD_IN,3);
	Bond bdi_4(BD_OUT,4);

	std::vector<Bond> bonds;
	bonds.push_back(bdi_2);
	bonds.push_back(bdi_3);
	bonds.push_back(bdi_4);

	UniTensor<double> T1(bonds);

	std::vector<double> Telem(24,0);
	for(size_t i=0; i < Telem.size(); i++){
		Telem[i]=i;
	}

	T1.SetElem(Telem);

	UniTensor<double> T2(T1);

	//EXPECT_EQ(T1.ElemNum(),T2.ElemNum());
	EXPECT_EQ(24,T2.ElemNum());
	EXPECT_EQ(T1.bond(),T2.bond());
}

TEST(tensor,Zeros){
	Bond bdi_2(BD_IN,2);
	Bond bdi_3(BD_IN,3);
	Bond bdi_4(BD_OUT,4);

	std::vector<Bond> bonds;
	bonds.push_back(bdi_2);
	bonds.push_back(bdi_3);
	bonds.push_back(bdi_4);

	UniTensor<double> T(bonds);

	std::vector<double> Telem(24,0);
	for(size_t i=0; i < Telem.size(); i++){
		Telem[i]=i;
	}

	T.SetElem(Telem);
	T.Zeros();
	for(size_t i=0; i < Telem.size(); i++)
		EXPECT_EQ(0,T[i]);
}

TEST(tensor,operaterplus){
	Bond bdi_2(BD_IN,2);
	Bond bdi_3(BD_IN,3);
	Bond bdi_4(BD_OUT,4);

	std::vector<Bond> bonds;
	bonds.push_back(bdi_2);
	bonds.push_back(bdi_3);
	bonds.push_back(bdi_4);

	UniTensor<double> T1(bonds);
	UniTensor<double> T2(bonds);

	std::vector<double> Telem(24,0);
	for(size_t i=0; i < Telem.size(); i++){
		Telem[i]=i;
	}

	T1.SetElem(Telem);
	T2.SetElem(Telem);
	T1+=T2;
	EXPECT_EQ(bonds,T1.bond());
	EXPECT_EQ(24,T1.ElemNum());
	for(size_t i=0; i < 24; i++){
		EXPECT_EQ(T1[i],i+i);
	}
}

/*TEST(tensor,operaterminus){
	Bond bdi_2(BD_IN,2);
	Bond bdi_3(BD_IN,3);
	Bond bdi_4(BD_OUT,4);

	std::vector<Bond> bonds;
	bonds.push_back(bdi_2);
	bonds.push_back(bdi_3);
	bonds.push_back(bdi_4);

	UniTensor<double> T1(bonds);
	UniTensor<double> T2(bonds);

	std::vector<double> Telem(24,0);
	for(size_t i=0; i < Telem.size(); i++){
		Telem[i]=i;
	}

	T1.SetElem(Telem);
	T2.SetElem(Telem);
	T1=T1-T2;
	EXPECT_EQ(bonds,T1.bond());
	EXPECT_EQ(24,T1.ElemNum());
	for(size_t i=0; i < 24; i++){
		EXPECT_EQ(0,T1[i]);
	}
}*/

/*TEST(tensor,operatermulti){
	Bond bdi_2(BD_IN,2);
	Bond bdi_3(BD_IN,3);
	Bond bdi_4(BD_OUT,4);

	std::vector<Bond> bonds;
	bonds.push_back(bdi_2);
	bonds.push_back(bdi_3);
	bonds.push_back(bdi_4);

	UniTensor<double> T1(bonds);
	UniTensor<double> T2(bonds);

	std::vector<double> Telem(24,0);
	for(size_t i=0; i < Telem.size(); i++){
		Telem[i]=i;
	}

	T1.SetElem(Telem);
	T2.SetElem(Telem);
	T1=T1*T2;
	EXPECT_EQ(bonds,T1.bond());
	EXPECT_EQ(24,T1.ElemNum());
	for(size_t i=0; i < 24; i++){
		EXPECT_EQ(T1[i],i+i);
	}
}*/

TEST(tensor,ChangeLabel){
	Bond bdi_2(BD_IN,2);
	Bond bdi_3(BD_IN,3);
	Bond bdi_4(BD_OUT,4);

	std::vector<Bond> bonds;
	bonds.push_back(bdi_2);
	bonds.push_back(bdi_3);
	bonds.push_back(bdi_4);

	UniTensor<double> T(bonds);

	std::vector<double> Telem(24,0);
	for(size_t i=0;i<Telem.size();i++)
		Telem[i]=i;
	T.SetElem(Telem);

	int label[]={0,1,2};
	int new_label[]={1,0,2};
	std::vector<int> l(label,label+3);
	std::vector<int> nl(new_label,new_label+3);
	EXPECT_EQ(l,T.label());
	T.SetLabel(new_label);
	EXPECT_EQ(nl,T.label());

}

TEST(tensor,CombineBond){
	Bond bdi_2(BD_IN,2);
	Bond bdi_3(BD_IN,3);
	Bond bdi_4(BD_OUT,4);
	Bond bdi_6(BD_IN,6);

	std::vector<Bond> bonds;
	bonds.push_back(bdi_2);
	bonds.push_back(bdi_3);
	bonds.push_back(bdi_4);
	//bonds.push_back(bdi_5);

	UniTensor<double> T(bonds);

	std::vector<double> Telem(24,0);
	for(size_t i=0;i<Telem.size();i++)
		Telem[i]=i;
	T.SetElem(Telem);

	int label[]={0,1,2};
	int new_label[]={0,1};

	std::vector<int> l(label,label+3);
	std::vector<int> nl(new_label,new_label+2);

	T.CombineBond(nl);

	EXPECT_EQ(24,T.ElemNum());
	EXPECT_EQ(2,T.BondNum());
	EXPECT_EQ(bdi_6,T.bond(0));
	EXPECT_EQ(bdi_4,T.bond(1));
}
TEST(tensor,Permute){
	Bond bdi_2(BD_IN,2);
	Bond bdi_3(BD_IN,3);
	Bond bdi_4(BD_OUT,4);

	std::vector<Bond> bonds;
	bonds.push_back(bdi_2);
	bonds.push_back(bdi_3);
	bonds.push_back(bdi_4);

	UniTensor<double> T(bonds);

	std::vector<double> Telem(24,0);
	for(size_t i=0;i<Telem.size();i++)
		Telem[i]=i;
	T.SetElem(Telem);
	int label[]={0,1,2};
	int new_label[]={1,0,2};

	Permute(T,new_label,2,INPLACE);

	std::vector<double> newelem(24,0);
	for(size_t i = 0; i < 3; i++){
		for(size_t j = 0; j < 2; j++){
			for(size_t k = 0; k < 4 ; k++){
				newelem[i * 8 + j * 4 + k] = j * 12 + i * 4 + k;
			}
		}
	}

	for(size_t i = 0; i < 24; i++)
		EXPECT_EQ(newelem[i],T[i]);
	EXPECT_EQ(T.bond(0),bdi_3);
	EXPECT_EQ(T.bond(1),bdi_2);
	EXPECT_EQ(T.bond(2),bdi_4);

	//std::cout<<T;
}
#ifdef HDF5
TEST(tensor,HDFIO){
	Bond bdi_2(BD_IN,2);
	Bond bdi_3(BD_IN,3);
	Bond bdi_4(BD_OUT,4);

	std::vector<Bond> bonds;
	bonds.push_back(bdi_2);
	bonds.push_back(bdi_3);
	bonds.push_back(bdi_4);

	UniTensor<double> T(bonds);

	std::vector<double> Telem(24,0);
	for(size_t i=0;i<Telem.size();i++)
		Telem[i]=i;
	T.SetElem(Telem);
	int label[]={0,1,2};
	T.SetLabel(label);
	T.H5Save("tensor.h5", "T");
	UniTensor<double> L;
	L.H5Load("tensor.h5", "T");
	for(size_t i=0; i < 24; i++){
		EXPECT_EQ(T[i],L[i]);
	}
	// EXPECT_EQ(L.bond(0),T.bond(0));// I don't know why this fails!
	// EXPECT_EQ(L.bond(1),T.bond(1));// Its outputs are exactly the same.
	// EXPECT_EQ(L.bond(2),T.bond(2));
}
#endif
#endif
