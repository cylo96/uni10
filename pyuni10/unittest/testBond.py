import pyuni10 as uni10
import unittest

class TestBond( unittest.TestCase ):

    def testConsrtuctorTypeDim( self ):
        bd = uni10.Bond( uni10.BD_IN, 100 )

        self.assertEqual( uni10.BD_IN, bd.type() )
        self.assertEqual( 100, bd.dim() )

    def testConstructorTypeQnums( self ):
        q1 = uni10.Qnum( 1 )
        q0 = uni10.Qnum( 0 )
        q_1 = uni10.Qnum( -1 )

        qnums = [q1, q1, q0, q0, q0, q_1]
        bd = uni10.Bond( uni10.BD_OUT, qnums )

        qlist = bd.Qlist()
        self.assertEqual( q1, qlist[0] )
        self.assertEqual( q1, qlist[1] )
        self.assertEqual( q0, qlist[2] )
        self.assertEqual( q0, qlist[3] )
        self.assertEqual( q0, qlist[4] )
        self.assertEqual( q_1, qlist[5] )

        degs = bd.degeneracy()
        keys = sorted(list( degs.keys() ))
        self.assertEqual( q_1, keys[0] )
        self.assertEqual( 1, degs[ keys[0] ] )
        self.assertEqual( q0, keys[1] )
        self.assertEqual( 3, degs[ keys[1] ] )
        self.assertEqual( q1,keys[2] )
        self.assertEqual( 2, degs[ keys[2] ] )

    def testCopyConstructor( self ):
        bd1 = uni10.Bond( uni10.BD_IN, 100 )
        bd2 = uni10.Bond( bd1 )

        self.assertEqual( bd1, bd2 )

    def testChangeBondType( self ):
        q1 = uni10.Qnum( 1 )
        bd = uni10.Bond( uni10.BD_IN, [q1, q1, q1] )
        bd.change( uni10.BD_OUT )
        self.assertEqual( uni10.BD_OUT, bd.type() )

        qlist = bd.Qlist()
        for i in range( 3 ):
            self.assertEqual( -1, qlist[i].U1() )

    def testDummyChangeBondType( self ):
        q1 = uni10.Qnum( 1 )
        bd = uni10.Bond( uni10.BD_IN, [q1, q1, q1] )
        bd.dummy_change( uni10.BD_OUT )
        self.assertEqual( uni10.BD_OUT, bd.type() )

        qlist = bd.Qlist()
        for i in range( 3 ):
            self.assertEqual( 1, qlist[i].U1() )

    def testCombine( self ):
        q2 = uni10.Qnum( 2 )
        q1 = uni10.Qnum( 1 )
        q0 = uni10.Qnum( 0 )
        q_1 = uni10.Qnum( -1 )
        q_2 = uni10.Qnum( -2 )

        qnums = [ q1, q1, q0, q0, q0, q_1 ]
        bd1 = uni10.Bond( uni10.BD_IN, qnums )

        qnums = [ q1, q0, q0, q_1 ]
        bd2 = uni10.Bond( uni10.BD_IN, qnums )
        bd2.combine( bd1 )

        degs = bd2.degeneracy()
        keys = sorted(list( degs.keys() ))

        self.assertEqual( uni10.BD_IN, bd2.type() )
        self.assertEqual( 24, bd2.dim() )

        self.assertEqual( q_2, keys[0] )
        self.assertEqual( 1, degs[ keys[0] ] )
        self.assertEqual( q_1, keys[1] )
        self.assertEqual( 5, degs[ keys[1] ] )
        self.assertEqual( q0, keys[2] )
        self.assertEqual( 9, degs[ keys[2] ] )
        self.assertEqual( q1, keys[3] )
        self.assertEqual( 7, degs[ keys[3] ] )
        self.assertEqual( q2, keys[4] )
        self.assertEqual( 2, degs[ keys[4] ] )


if __name__ == "__main__":
    unittest.main()
