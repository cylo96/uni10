import pyUni10 as uni10
import numpy as np
import unittest

from testMatrix import sizes ## Size of matrix.
from testMatrix import test_elem_not_diag
from testMatrix import test_elem_diag
from testMatrix import suffix

class TestMatrixd( unittest.TestCase ):
    
    def testConstructorFromSize_notDiag( self ):
        for size in sizes:
            M = uni10.MatrixR( size.row, size.col )

            self.assertEqual( size.row, M.row() )
            self.assertEqual( size.col, M.col() )
            self.assertFalse( M.diag() )

    def testConstructorFromSize_Diag( self ):
        for size in sizes:
            M = uni10.MatrixR( size.row, size.col, True )

            self.assertEqual( size.row, M.row() )
            self.assertEqual( size.col, M.col() )
            self.assertTrue( M.diag() )

    def testConstructorFromNumpy_notDiag( self ):
        for size in sizes:
            src = np.array( [float( x ) for x in range( size.row * size.col )] )
            src = src.reshape( ( size.row, size.col ) )
            M = uni10.MatrixR( src )

            self.assertEqual( size.row, M.row() )
            self.assertEqual( size.col, M.col() )
            self.assertFalse( M.diag() )
            elem = M.GetElem()
            src = src.flatten()
            for i in range( size.row * size.col ):
                self.assertEqual(src[i], elem[i])

    def testConstructorFromFile_notDiag( self ):
        for i in range(len(suffix)):
            M = uni10.MatrixR("testMatrixd_notDiag{}".format(suffix[i]))

            self.assertEqual(sizes[i].row, M.row())
            self.assertEqual(sizes[i].col, M.col())
            self.assertFalse(M.diag())
            elem = M.GetElem()
            src = test_elem_not_diag[i]
            for j in range(len(src)):
                self.assertEqual(src[j], elem[j])

    def testConstructorFromFile_Diag(self):
        for i in range(len(suffix)):
            M = uni10.MatrixR("testMatrixd_Diag{}".format(suffix[i]))

            self.assertEqual(sizes[i].row, M.row())
            self.assertEqual(sizes[i].col, M.col())
            self.assertTrue(M.diag())
            elem = M.GetElem()
            src = test_elem_diag[i]
            for j in range(len(src)):
                self.assertEqual(src[j], elem[j])

if __name__ == "__main__":
    unittest.main()
