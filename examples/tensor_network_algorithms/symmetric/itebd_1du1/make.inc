CXX           := g++
CXXFLAGS      := -m64 -O2 -std=c++11

UNI10_ROOT    := $(UNI10_ROOT_CPU)
UNI10CXXFLAGS := -DUNI_CPU -DUNI_LAPACK
UNI10_VERSION := lapack_cpu
