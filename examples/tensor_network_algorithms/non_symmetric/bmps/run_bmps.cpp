#include "../../hamiltonian/hamiltonian.h"
#include "bmps_tools/bmps.h"

using namespace std;
using namespace uni10;

int main(){
  
  Uni10Create();
  Uni10PrintEnvInfo();

  bmps_paras paras;
  paras.Load_bmps_paras();

  UniTensor<uni10_double64> hamiltonian_d;
  UniTensor<uni10_complex128> hamiltonian_c;

  bool is_real = load_hamiltonian(hamiltonian_d, hamiltonian_c);

  if(is_real){
    Bmps<uni10_double64> bmps_run(hamiltonian_d, paras);
    bmps_run.initialize("./tensors/");
    bmps_run.Optimize();
    cout<<endl<<"converged energy = "<<setprecision(10)<<bmps_run.measureExpec(hamiltonian_d)<<endl;
  }
  else{
    Bmps<uni10_complex128> bmps_run(hamiltonian_c, paras);
    bmps_run.initialize("./tensors/");
    bmps_run.Optimize();
    cout<<endl<<"converged energy = "<<setprecision(10)<<bmps_run.measureExpec(hamiltonian_d)<<endl;
  }

  Uni10Destroy();

  return 0;
}
