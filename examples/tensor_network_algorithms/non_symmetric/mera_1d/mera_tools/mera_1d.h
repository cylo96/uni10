#ifndef __MERA_1D_H__
#define __MERA_1D_H__

#include "uni10.hpp"
#include "../../../../operator/operator.h"

using namespace std;
using namespace uni10;

struct mera_paras{

  mera_paras(){
    rank=1; chi=3; dim=2; layers=5; 
    epsilon = 1.e-8;
    q_tensor=1; q_layer=1;
  }

  void load_mera_paras(){

    FILE* rcfp = fopen(".merarc", "r");

    int max_len = 256;
    char buffer[max_len];


    char* pch;
    while(fgets(buffer, max_len, rcfp)){

      pch = strtok(buffer, ":");
      pch = strtok(NULL, ":");

      if(strcmp ("rank", buffer)==0)
        rank = atoi(pch);

      else if(strcmp ("chi", buffer)==0)
        chi = atoi(pch);

      else if(strcmp ("d", buffer)==0)
        dim = atoi(pch);

      else if(strcmp ("layers", buffer)==0)
        layers = atoi(pch);

      else if(strcmp ("epsilon", buffer)==0)
        epsilon = atof(pch);

      else if(strcmp ("q_tensor", buffer)==0)
        q_tensor = atoi(pch);

      else if(strcmp ("q_layer", buffer)==0)
        q_layer = atoi(pch);

      else if(buffer[0] =='#' || pch == NULL)
        continue;

      else{
        fprintf(stdout, "%s", "Setting the parameters with wrong names.");
        exit(0);
      }

    }

    fclose(rcfp);

  }

  void print_info() const{

    fprintf(stdout, "=====================================\n");
    fprintf(stdout, "|        Parameters of MERA_1D      |\n");
    fprintf(stdout, "=====================================\n");
    fprintf(stdout, "# rank   : %d\n", rank);
    fprintf(stdout, "# chi    : %d\n", chi);
    fprintf(stdout, "# d      : %d\n", dim);
    fprintf(stdout, "# layers : %d\n", layers);

    if(epsilon > 0)
      fprintf(stdout, "# eps    : 1e%.1f\n", log10(epsilon));
    else
      fprintf(stdout, "# eps    : %.2f\n", epsilon);

    fprintf(stdout, "=====================================\n");

  }

  int rank, chi, dim, layers, q_tensor, q_layer;     // D: Virtual bonds dimension.
                       // d: Physical bonds dimension.
                       // max_D: Upper bondary of vitual bonds dimesnion.
  double epsilon;

};


template<typename T>
class MERA_1D{
    private:
        int rank;       // contravariant dimension of top tensor
        int chi;        // dominant bond dimension
        int dim;        // physical dimension
        int layers;     // number of layers (tau), 0, 1, .., layers-1
                        // this is where the disentangler and coarsegrainer live
        int lattices;   // indices of layer of lattice, 0, 1, .., layers
                        // this is where the hamiltonian and densitymatrix live
        int sites;      // number of sites, equal 2*3^(layers-1)
        double epsilon; // convergence criterion
        int q_tensor;
        int q_layer;

        UniTensor<T>* disentangler;    // unitary
        UniTensor<T>* coarsegrainer;   // isometry
        UniTensor<T>  toptensor;
        UniTensor<T>* hamiltonian;
        UniTensor<T>* densitymatrix;
        UniTensor<T>* sigmaz;          // spontaneous magnetization

        Matrix<T> Pz;
        Matrix<T> Px;
        Matrix<T> Sp;
        Matrix<T> Sm;
        Matrix<T> Ident;
        static bool comparator (const pair<size_t,double>& l, const pair<size_t,double>& r){
            return l.second < r.second;
        }

        map<string, Network*> net_list;

    public:

        MERA_1D(const UniTensor<T>& _H, const mera_paras& paras, const map<string, Network*>& net_list);
        ~MERA_1D();
        
        UniTensor<T> Ascend(int tau, const UniTensor<T>& Obs); 
        UniTensor<T> Descend(int tau, const UniTensor<T>& Dens);
        void OptW(int tau);
        void OptU(int tau);
        void Optimize();
        double Expectation();

        void Test();

};
#endif
